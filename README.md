# Befreit Muenchens oBikes

... und das komplett ohne Sachbeschaedigung, innerhalb
weniger Sekunden pro obike.  Hilf uns, die vor einiger Zeit von
[librebike.info](https://librebike.info/de/) ausgerufene Vision von
freier und nachhaltiger Fortbewegung noch dieses Jahr in deiner Stadt
zur Realitaet zu machen.

Du bietest den Menschen um dich herum nicht nur einen Mehrwert, du
sparst auch der Stadt viel Geld und hilfst, viele Tonnen Schrott
zu vermeiden.  Wir hoffen ausserdem, dass eine Weiternutzung der Raeder
durch die Oeffentlichkeit denjenigen Mitbuergern zu denken gibt, die
bisher aus blosser Zerstoerungswut oBikes kaputt machen (oder sie gar
in unsere Fluesse werfen).

Wir haben in Muenchen schon einige hundert Raeder entsperrt. Hilf uns,
alle restlichen Obikes zu befreien!

## obike-unlock

Die hier angebotene Software kommuniziert (unter einigen Voraussetzungen,
siehe unten) mit dem oBike-Server und per Bluetooth mit dem Schloss
am Fahrrad.  Diese Methode funktioniert nur bei obikes, deren Schloss
noch funktionstuechtig ist. Dies laesst sich einfach ueberpruefen,
indem man den Knopf an der Seite des Schlosses betaetigt. Das Schloss
sollte mit einem deutlichen Pieps-Ton reagieren.

Technische Details sind in der sehr ausfuehrlichen [Dokumentation von
@atoinet](https://github.com/antoinet/obike/blob/master/README.md)
zu finden.

Das Ziel dieses Scripts ist es, das entsperren von obikes moeglichst
einfach zu machen. Es reicht aus, wenn man sich 1-2 Meter neben das obike
stellt. Das Schloss springt nach 5-30 Sekunden ohne jegliche Interaktion
einfach auf.

### Anforderungen

* ein oBike-Account (es muss kein Zahlungsmittel verknuepft werden; kann ueber die App registriert werden)
* ein Bluetooth 4.0 LE (Low Energy) faehiges Bluetooth-Modul
  (z.B. USB oder das im Macbook verbaute). Alle modernen Laptops sollten
  funktionieren.
* mobiles Internet, z.B. per Hotspot via Smartphone
* Falls du ein Macbook hast oder aus einem anderen Grund eine virtuelle Maschine verwenden willst:
    * VirtualBox: https://www.virtualbox.org/
    * dieses [kali Linux Image fuer VirtualBox](https://www.offensive-security.com/kali-linux-vm-vmware-virtualbox-image-download/) ("Kali Linux VirtualBox Images"-Tab auswaehlen -> "Kali Linux Vbox 64 Bit Ova")
* Falls du schon Linux auf deinem Laptop (o.ae.) hast:
    * ... weisst du vermutlich (hoffentlich) eh was du tust.
    * die Software-Pakete: ```polipo, tor, tmux, bluez```. Das Start-Script wird versuchen sie per ```apt-get``` zu installieren.

Getestet wurde dieses Setup bisher unter folgenden Systemen:

* Macbook Pro, Early 2015 (Broadcom 20703A1 Bluetooth) + VirtualBox (siehe oben).

### Setup VirtualBox
1. Nachdem die Maschine importiert wurde (einfach die OVA-Datei oeffnen)
2. In den Einstellungen fuer die neue virtuelle Maschine muss nun der Bluetooth-Controller weitergereicht werden. Die Einstellung ist unter "Ports" -> "USB" zu finden.
3. virtuelle Maschine starten
4. dieses Repository clonen: ```git clone https://github.com/freebikes/obike.git && cd obike```
6. obike Login-Daten in der Datei ```obike-login.txt``` in folgendem Format eintragen: ```laendervorwahl:nummer:passwort```. Also zum Beispiel: ```+49:175123456:foo```.
7. mit ```./start.sh``` alle notwendigen Komponenten installieren & starten. Es sollten bereits jetzt Meldungen wie "not an obike" angezeigt werden, falls ein anderes Bluetooth-LE-Geraet in deiner Naehe ist (was eigentlich immer der Fall ist).
8. oBike suchen, daneben bzw dahinter stellen bzw. Laptop in die Naehe des Schlosses halten.


Viel Spass! :)


### Technisches Blabla

- Es gibt einen Schwellenwert fuer die Empfangsstaerke (rssi), unterhalb dem wir gar nicht erst versuchen, uns mit dem obike zu verbinden. Das ist notwendig, da wir sonst dauernd versuchen wuerden uns mit obikes zu verbinden, deren LE-Beacon wir empfangen haben, mit denen wir aber nicht wirklich kommunizieren koennen. Dies fuehrt zu zu vielen Timeouts und wuerde das Script unbenutzbar machen. Weiterhin ist es nuetzlich, um hintereinander mehrere obikes zu entsperren, die in kurzer Entfernung voneinander entfernt stehen.
- Das Script beendet sich, sobald ein Bluetooth-Fehler aufgetreten ist. Es wird nach einer Controller-(Neu-)-Initialisierung automatisch wieder gestartet. Es passiert leider (auf bestimmten Hardware/-Softwarekombinationen) sehr haeufig, dass das Bluetooth-Subsystem durch das wiederholte scan/connect so schlimm verwirrt wird, dass der Controller neu initialisiert werden muss (hciconfig im start-Script).
- Manche obikes muessen mehrmals durch das Script entsperrt werden, bevor sich das Schloss oeffnet.
- Manche obikes senden immer eine Challenge, die 0 ist. Keine Ahnung warum. Diese obikes lassen sich nicht entsperren.
- Manche obikes sind nicht in der lock_db.txt eingetragen. Ich habe es nicht geschafft, diese obikes mit einer zufaelligen Fahrradnummer zu entsperren, wie es der originale PoC getan hat.

# Credits

Ermoeglicht wurde dies durch einzig und allein durch die
Arbeit des schweizer IT-Sicherheitsforschers
[@antoinet](https://github.com/antoinet/obike). Vielen Dank & gute
Arbeit! Antoinet hat in keinster Weise mit diesem Aufruf/Projekt zu tun.

Un-Credits: anstatt euch ein Fahrrad von Radlbauer zu kaufen, nehmt lieber ein kostenloses oBike. Die Qualitaet ist relativ gleich.

# Contribute!

- Das Script ist in der Entwicklungsphase und koennte stellenweise als "dirty hack" bezeichnet werden.  Pull-requests sind willkommen.
- Auch Wenn du nicht programmieren kannst, kannst du helfen: an vielen
Orten in der Stadt liegen grosse Haufen obikes, viele davon ohne Sattel,
aber inzwischen (teilweise durch uns) entsperrt. Nimm ein Sattel von
einem Rad mit Schloss, das sich nicht mehr oeffnen laesst (kein Pieps
beim Druecken des Knopfs am Schloss) und stecke ihn auf ein Rad, das
ansonsten fahrtauglich ist.
- Sag Bescheid, wenn du weisst, wo es in Muenchen noch grosse (und kleine) obike-Haufen gibt, damit wir sie entsperren koennen.
