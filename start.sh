#!/bin/sh
set -x

apt-get install -y tmux tor polipo
service tor restart
service bluetooth restart
cp -f polipo.conf /etc/polipo/config
service polipo restart

date > /tmp/obike.log
# uncomment for remote logging, e.g. for smartphone viewing
#tmux new-session "unbuffer tail -f /tmp/obike.log | ssh user@somehost 'cat >> /tmp/obike.log'"
while true; do
	hciconfig hci0 down
	hciconfig hci0 up
	HTTPS_PROXY=http://localhost:8123 unbuffer ./unlock_obike.py 2>&1 | tee -a /tmp/obike.log
	sleep 1
done

