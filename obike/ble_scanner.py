from bluepy import btle
from hexdump import hexdump
from obike.lockdb import lockdb

# FIXME a global that is imported in __main__...
db = {}

class BleScanner(object):

    class ScanDelegate(btle.DefaultDelegate):

        def __init__(self):
            btle.DefaultDelegate.__init__(self)

        def handleDiscovery(self, dev, isNewDev, isNewData):
            global db
            if isNewDev:
                name = False
                try:
                    name = dev.getValueText(9)
                except UnicodeEncodeError:
                    name =  "(encoding error)"
                lockno = name[5:14]
                bikeno = lockdb.lookup(lockno)
                if name and name.startswith('bike:'):
                    print " [-] %s (%s, %s)" % (bikeno, dev.addr, name)
                    db[dev.addr] = bikeno


    def __init__(self, iface=0):
        self.scanner = btle.Scanner(iface).withDelegate(self.ScanDelegate())

    def scan(self, t=10):
        print "[*] scanning (%ds)..." % t
        devices = self.scanner.scan(t)
        #return [dev for dev in devices if dev.addr.upper().startswith('D4')]
        return devices

